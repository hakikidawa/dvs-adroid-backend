<?php
	/*
		Android Backend Access
		name: Steven Edward
		date: 3/05/2015
		
		
	*/
	
	//This is a new test commit
	/*
		DB Connection
	*/
	
	$dbHost     = 'localhost';
	$dbUser     = 'root';
	$dbPass     = '';
	$dbName     ='lab_dvs';
	$dbConn = mysql_connect ($dbHost, $dbUser, $dbPass) or die ('MySQL connect failed. ' . mysql_error());
	@mysql_select_db($dbName) or die('Cannot select database. ' . mysql_error());
	
	/*
		The DHC header
	*/
	
	
	//you must set $_POST['mobile']
	
	$userMob = (isset($_POST['mobile']) && $_POST['mobile'] != '') ? $_POST['mobile'] : '';
	
	if($userMob  == $mobileV){
		$process = $_POST['type'];
		$lang    = $_POST['lang']; //en:english; sw:kiswahili
		$status = 1;
		
        if($process == 'login'){
            $username = $_POST["username"];
            $password = $_POST["password"];

            $verification_login = verify_mobile_user($username,$password);
			//$sqllogin = "SELECT id,token FROM tbl_pharmacy WHERE status='1' AND email='$username' AND password = MD5('$password')";
			//$result = mysql_query($sqllogin);
            if(count($verification_login)>0){
                $message['status'] = 'success';
                $token = createToken($verification_login['id']);
                $message['token'] = $token;
				
            }else{
                $message['status']  = 'failed';
				$msgCode = 'mobileLogFail';
				$message['message'] = getMessageDescription($msgCode,$lang);
            }

        }else if($process == 'verification'){
            $code = $_GET["code"];
			$verification = mobilev2_drug_verify($code);
			$message['status'] = 'ready';
            $message['message'] ='box already activated';
			
		}else if($process == 'activation'){
			$code     = $_GET["code"];
            $token    = $_GET["token"];
            $username = $_GET["username"];
            $check_token = verifyToken($username,$token);
            if(count($check_token)>0){

                $check_batch = checkBox($code);
                if($check_batch){

                    $verification = activateBox($code);
                    if($verification){

                        $message['status'] = 'success';
						$msgCode = 'mobileVerificationSuccess';
                        $message['message'] = getMessageDescription($msgCode,$lang);

                    }else{
						
                        $message['status'] = 'ready';
						$msgCode = 'mobileVerificationReady';
                        $message['message'] = getMessageDescription($msgCode,$lang);
                    }
                }else{
                    $message['status'] = 'failed';
					$msgCode = 'mobileVerificationFailed';
                    $message['message'] = getMessageDescription($msgCode,$lang);
                }

            }else{
                $message['status'] = 'logout';
				$msgCode = 'mobileVerificationFailedLogin';
                $message['message'] = getMessageDescription($msgCode,$lang);
            }
		}else if($process == 'healthTips'){
			$message['message'] = mobile_health_tips();
			//echo json_encode($message);
			
		}elseif($process == 'listPharmacy'){
			$message = mobile_model_list_pharmacy();
		}
		//$message = $this->dvs_model->mobile_model_list_pharmacy();
		echo json_encode($message);
	}else{
		$message['message'] = "No direct script access allowed";
		echo json_encode($message);
	}

	//functions from dvs_model
	
    function createToken($user_id){
        $token = "";
        for($x = 0; $x < 30; $x++){
            $opt = rand(0,3);
            switch($opt){
                case "0":
                    $token .= chr(rand(0,25)+65);
                    break;

                case "1":
                    $token .= rand(0,9);

                default:
                    $token .= chr(rand(0,25)+97);
            }
        }

		mysql_query("UPDATE tbl_pharmacy SET token='$token' WHERE id='$user_id'");

        return $token;
    }
	
	//verify token
	function  verifyToken($username,$token){
        $status = 1;
		$sql = "SELECT id,token FROM tbl_pharmacy status='$status' AND email='$username' AND token='$token'";
        $result = mysql_query($sql);
        return mysql_fetch_assoc($result);
    }
	
	function verify_mobile_user($username,$password){
        $status = 1;
        $sqllogin = "SELECT id,token FROM tbl_pharmacy WHERE status='1' AND email='$username' AND password = MD5('$password')";
		$result = mysql_query($sqllogin);
        return mysql_fetch_assoc($result);
    }
	
	function getMessageDescription($msgCode,$lang){
        //$this->db->update('tbl_scratch_code',$code_changes,array('id' => $code_id));
		$sql = "SELECT Content_sw,Content_en FROM tbl_message_template
				WHERE Type='$msgCode'";
		$query = mysql_query($sql);
		
		if(mysql_num_rows($query)>0){
			$row = mysql_fetch_assoc($query);
			if($lang == 'en'){
				return $row['Content_en']; 
			}else{
				return $row['Content_sw']; 
			}
		}else{
			return "Language Not Found";
		}
    }
	
	//Medical drug verification
	function mobilev2_drug_verify($code){
		$status = 1;
		$sql = "SELECT md.name drug_name, md.Description drug_description, b.expire_date drug_expire_date, IF(b.expire_date>CURDATE(),'drugNotExpired','drugExpired') drug_expiration_status, IF(sc.status=1,'drugGenuine','drugRevalidation') drug_code_status, IF(b.status=1,'batchActive','batchInActive') batch_status
				FROM tbl_medical_drug md, tbl_medical_drug_manufacturer mdm, tbl_order_information oi, tbl_batch b, tbl_batch_boxes bb, tbl_scratch_code sc
				WHERE sc.box_id = bb.id
				AND bb.batch_id = b.id
				AND b.oder_information_id = oi.id
				AND oi.medical_drug_id = mdm.id
				AND mdm.drug_id = md.id
				AND sc.scratch_number = '$code'";
				
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	
	//Mobile list pharmacies
	function mobile_model_list_pharmacy(){
		$sql = "SELECT p.id pharmacy_id, p.name pharmacy_name, p.email email, p.phone_number phone, p.alt_phone_number alt_phone, l.latitude latitude, l.longitude longitude
				FROM tbl_pharmacy p, tbl_location l
		        WHERE p.location_id=l.id";
		$result = mysql_query($sql);
		$myArray = array();
		$i=0;
		while($row = mysql_fetch_assoc($result)){
			$myArray[$i]['pharmacy_id'] = $row['pharmacy_id'];
			$myArray[$i]['pharmacy_name'] = $row['pharmacy_name'];
			$myArray[$i]['email'] = $row['email'];
			$myArray[$i]['phone'] = $row['phone'];
			$myArray[$i]['alt_phone'] = $row['alt_phone'];
			$myArray[$i]['latitude'] = $row['latitude'];
			$myArray[$i]['longitude'] = $row['longitude'];
			$i++;
		}
        return $myArray;
	}
	
	//check box
	function checkBox($code){
		$sql = "SELECT id  FROM tbl_batch_boxes WHERE MD5(box_number)='$code'";
        $query = $this->db->query($sql);
        $batch = $query->row_array();

        if(count($batch)>0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
	
	function activateBox($code){
		
		$sql = "SELECT rt.id, rt.status, rt.tag_info FROM tbl_batch_boxes bb, tbl_rfid r, tbl_rfid_tag rt
				WHERE bb.id=r.box_id AND r.rfid_tag_id = rt.id AND MD5(box_number)='$code'";
				
		$query = mysql_query($sql);
        $box   = mysql_fetch_assoc($query);
		
        if(count($box)>0){
            if($box['status'] == 0){
			   $sql = "UPDATE tbl_rfid_tag SET status='1' WHERE tag_info='{$box['tag_info']}'";			   
			   mysql_query($sql);
               return TRUE;
   
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }

    }
	
	//List of medical health tips
	function mobile_health_tips(){
		$sql = "SELECT * FROM mobile_health_tips";
		$query = mysql_query($sql);
		return mysql_fetch_assoc($query);
	}
	
	
	//adroid_dvs@hakikidawa#for_life
?>